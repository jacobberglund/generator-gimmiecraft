'use strict';

/*

	gulp:
	fixa en template task.

	nice to have:
	csslint, jslint?
	readme?
	https://github.com/GoogleChrome/sw-precache

 */

const Generator = require('yeoman-generator');
const path = require('path');
const fs = require('fs-extra');
const chalk = require('chalk');
const pleasant = require('pleasant-progress');
const download = require('download');
const decompress = require('decompress');
const child_process = require('child_process');

const INSTALLATION_QUESTIONS = [
	{
		type    : 'input',
		name    : 'siteName',
		message : 'Your project name. Whats it gonna be?',
		default : 'mycraftsite'
	}, 
	{
		type    : 'confirm',
		name    : 'downloadCraft',
		message : 'Would you like to me to download Craft for you? (recommended)',
		default	: true
	},
	// Maybe add a "when" here? Ask if the sitename should be used for all options
	{
		type    : 'input',
		name    : 'localUrl',
		message : 'Cool. Hostname for development? (example: mycraftsite.dev)'
	}, 
	{
		type    : 'input',
		name    : 'server',
		message : 'Oakin Nuggins! Now, database servername?',
		default : 'localhost'
	}, 
	{
		type    : 'input',
		name    : 'dbName',
		message : 'Got it. Database name?'
	}, 
	{
		type    : 'input',
		name    : 'dbPrefix',
		message : 'Database table prefix?',
		default	: 'craft'
	}, 
	{
		type    : 'input',
		name    : 'dbUser',
		message : 'User?',
		default	: this.dbName
	}, 
	{
		type    : 'input',
		name    : 'dbPassword',
		message : 'Password? I wont tell anyone, I promise. I will however write it out in plain text in the file db.php',
		default	: this.dbName
	}, 
	{
		type    : 'input',
		name    : 'adminPanel',
		message : 'Aight. Now, let us set a different url for the admin panel once the site goes live. Security by obscurity, you feel me?',
		default : 'publisering'
	}, 
	{
		type    : 'confirm',
		name    : 'addGit',
		message : 'Love it, cupcakes. You need git? I will run git init, add everything done in this install and commit should you choose to.'
	}
];

const QUICK_INSTALL_QUESTIONS = [
	{
		type		:	'confirm',
		name		: 'confirmQuickInstall',
		message	: 'Does this look OK?',
		default	: true
	},
	{
		when: function(response){
			if(!response.confirmQuickInstall){
				// if this aint okay, we'll exit Yeoman/Node
				process.exit();
			}
		}
	}
];

const SHARED_QUESTIONS = [
	{
		type		:	'confirm',
		name		: 'installPlugins',
		message	: 'I got a list with Craft plugins. Want to take a look?',
		default	: false
	},
	{
		when: function(response){
			return response.installPlugins;
		},
		type		: 'checkbox',
		name		: 'plugins',
		message	: 'Take your pick',
		choices	: [
			{
				name: 'SEOmatic',
				value: { url: 'https://github.com/nystudio107/seomatic.git', name: 'SEOmatic'}
			}
		]
	},
	{
		type		:	'confirm',
		name		: 'installWebModules',
		message	: 'And maybe some wpm (npm) modules?',
		default	: false
	},
	{
		when: function(response){
			return response.installWebModules;
		},
		type		: 'checkbox',
		name		: 'webmodules',
		message	: 'Take your pick',
		choices	: [
			{
				name :	'Lory.js slider',
				value:	'lory'
			},
			{
				name :	'Slick slider',
				value:	'slick-carousel'
			},
			{
				name :	'Anime.js',
				value:	'animejs',
				checked: true
			},
			{
				name :	'Lazysizes',
				value:	'lazysizes',
				checked: true
			},
			{
				name	:	'Barba.js',
				value	: 'barba.js',
				checked: true
			}
		]
	},
	{
		type		:	'confirm',
		name		: 'ready',
		message	: 'That was all. Ready?',
		default	: true
	},
]


var downloadFile = function(instructions){
	const progress = new pleasant();
	// Expects url, filename and destination
	// All files will be downloaded to ".tmp", which will be deleted once this installation is complete
	if (!fs.existsSync('.tmp')){
		fs.mkdirSync('.tmp');
	}

	return download(instructions.url).on('response', res => {
				progress.start('Downloading');
	}).then(data => {
		fs.writeFileSync('.tmp/' + instructions.filename, data);
		console.log('\n' + chalk.green.bold('+ ' +instructions.filename+ ' downloaded'));
		progress.stop();
		return instructions;
	});
};

var decompressFiles = function(instructions){
		// Expects filename. Working out of the folder set from answers.appName.
		console.log(chalk.green.bold('> Decompressing ' + instructions.filename));
		return decompress('.tmp/' + instructions.filename, instructions.dest).then(files => {
			console.log(chalk.green.bold('+ ' + instructions.filename + ' has been decompressed into ' + instructions.dest));
		});
};

module.exports = class extends Generator {

	constructor(args, opts){
		super(args, opts);

		// makes 'yo gimmiecraft mysitename' work as a quickinstall.
		this.argument('quickSiteName', {type: String, required: false });

		// These are not used unless it's a quick install.
		this.quickInstall = { 
			siteName: this.options.quickSiteName,
			downloadCraft: true,
			localUrl: this.options.quickSiteName + '.dev',
			server: 'localhost',
			dbName: this.options.quickSiteName,
			dbPrefix: 'craft',
			dbUser: this.options.quickSiteName,
			dbPassword:this.options.quickSiteName,
			adminPanel: 'publisering',
			addGit: true 
		};

	}

  initializing() {
		console.log(chalk.dim('\nStep 1: Initalizing \n') + chalk.white.bold('Hello and welcome. We are about to set up a new Craft site for you. \n'));
		const done = this.async();

		// Check whether we should use the normal or quick install questions.
		this.questions = this.options.quickSiteName != undefined ? QUICK_INSTALL_QUESTIONS : INSTALLATION_QUESTIONS;

		SHARED_QUESTIONS.forEach((i, index) =>{
			this.questions.push(i);
		});

		done();
  }

	prompting() {
		console.log(chalk.dim('Step 2: Prompting \n') + chalk.white.bold('First we gonna ask you a few questions \n'));
		const done = this.async();


		if(this.options.quickSiteName){
			console.log('Recieved ' +chalk.green.bold(this.options.quickSiteName) +' as an argument.\n\nThe config will look as such: ');
			for(var key in this.quickInstall){
				console.log(key +': '+ chalk.green.bold(this.quickInstall[key]));
			}
		} 

		return this.prompt(this.questions).then((answers) => {

			if(answers.confirmQuickInstall !== undefined ){
				this.answers = Object.assign(this.quickInstall, answers);
			} else {
				this.answers = answers;
			}

			done();
		});

	}

	configuring(){
		console.log(chalk.dim('Step 2: Configuring \n') + chalk.cyan.bold('Based on your answers we will go ahead and configure some things. Please wait.'));

		const done = this.async();
		const dir = this.answers.siteName;

		// If the directory doens't exist, make it
		if (!fs.existsSync(dir)){
				this.log('+ Creating this sites install folder ' + chalk.green(dir));
				fs.mkdirSync(dir);
		} else {
				this.log('> This sites folder already exists, what shennanigans are you up to? Well, I will install it into ' + chalk.green(dir) + ' anyway.');
		}

		this.destinationRoot(this.destinationRoot() + "/" + dir);

		if(this.answers.downloadCraft){
			if (!fs.existsSync(this.destinationRoot() + '/.tmp/craft')) {
				this.log('> Gonna request Craft to be downloaded. This wont take long...');
				
				downloadFile({filename: 'Craft.zip', url: 'http://buildwithcraft.com/latest.zip?accept_license=yes', dest:'.tmp'}).then( decompressFiles ).then(function(){
					done();
				});

			} else {
				console.log('> Good news! I found a downloaded version. Skipping.');
				done();
			}

		} else {
			done();
		}
	}

	writing(){
		console.log(chalk.dim('Step 3: Writing \n') + chalk.cyan.bold('Time to write stuff to your storage unit!'));

		const done = this.async();

		const FOLDERS = [
			{src:'boilerplates/app', dest: 'app'},
			{src:'boilerplates/src', dest: 'src'},
			{src:'boilerplates/gulpfiles/', dest: ''}
		];


		FOLDERS.forEach((folder, index) => {
			console.log('+ Folder ' + folder.src + ' moved to ' + folder.dest)

			try {
				fs.copySync(
					this.templatePath(folder.src), 
					this.destinationPath(folder.dest)
				);
			} catch (err) {
				console.log(err);	
			}
		});

		const FILES_TO_COPY = [
			{src:'_gitignore', dest: '.gitignore'}
		];

		FILES_TO_COPY.forEach((file, index) => {
			console.log('+ Copied ' + file.src + ' to ' + file.dest);
			
			try {
				fs.copySync(
					this.templatePath(file.src), 
					this.destinationPath(file.dest)
				);
			} catch (err) {
				console.log(err);	
			}
		});


		const TEMPLATES = [
			{src:'_package.json', dest: 'package.json'},
			{src:'_gulp.config.js', dest: 'gulp/gulp.config.js'},
			{src:'_db.php', dest: 'app/config/db.php'},
			{src:'_general.php', dest: 'app/config/general.php'},
			{src:'_robots.txt', dest: 'app/public/robots.txt'}
		];

		TEMPLATES.forEach( (template, index) => {
			console.log('+ Adding template ' + template.dest);
			this.fs.copyTpl(
				this.templatePath(template.src),
				this.destinationPath(template.dest),
					this.answers
				);
		});

		if(this.answers.downloadCraft){
			console.log('> Hold one, moving Craft');
			try {
				fs.copySync(
					this.destinationPath('.tmp/craft/app'),
					this.destinationPath('app/craft/app/')
				);
				console.log('+ Rejoice! Craft moved successfully!')		
			} catch(err) {
				console.log(err);
			}
		}

		done();
	}

	install(){
		console.log(chalk.dim('Step 4: Installing \n') + chalk.cyan.bold('Permissions, NPM, WPM, Git'));

		console.log(chalk.green('> Setting global permissions to 755 / 644'));
		child_process.execSync('chmod -R 755 *');
		child_process.execSync('find . -type f -exec chmod 644 {} \\;');

		console.log(chalk.green('> Setting permissions on craft/app to 775 / 644'));
		child_process.execSync('chmod -R 775 app/craft/app');
		child_process.execSync('find app/craft/app/ -type f -exec chmod 664 {} \\;');

		console.log(chalk.green('> Setting permissions on craft/config to 775 / 644'));
		child_process.execSync('chmod -R 775 app/config');
		child_process.execSync('find app/config/ -type f -exec chmod 664 {} \\;');

		console.log(chalk.green('> Setting permissions on craft/storage to 775 / 644'));
		child_process.execSync('chmod -R 775 app/craft/storage');
		child_process.execSync('find app/craft/storage/ -type f -exec chmod 664 {} \\;');	

		if (fs.existsSync('package.json')) {
			console.log(chalk.green('> Running npm install'));
			// --save make sure we're setting most recent versions on package.json
			child_process.execSync('npm install --save');
		}

		if(this.answers.installWebModules){
			console.log(chalk.green('> Running wpm install') + this.answers.webmodules.join(' '));
			child_process.execSync('wpm install ' + this.answers.webmodules.join(' ') + ' --save');
		}

		if(this.answers.addGit){
			console.log(chalk.green('> Initializing local git repository'));
			child_process.execSync('git init');

			if(this.answers.installPlugins){
				this.answers.plugins.forEach((plugin) => {
					console.log('+ ' + chalk.green(plugin.name) + ' added as submodule');
					child_process.execSync('git submodule add -f ' + plugin.url + ' app/plugins/' + plugin.name.toLowerCase());
				});
			}

			console.log(chalk.green('> Adding project files to git repository'));
			child_process.execSync('git add -A');

			console.log(chalk.green('> Doing initial commit to git repository'));
			child_process.execSync('git commit -m "Initial commit"');
		}
	}

	end(){
		console.log(chalk.dim('Step 5: Wrapping up \n') + chalk.cyan.bold('Taking care of the last items on the agenda.'));
		if (fs.existsSync(this.destinationRoot() + '/.tmp')) {
			console.log('> Cleaning up');
			fs.remove(this.destinationRoot() + '/.tmp');
		}

		console.log('++ That is all. ' + this.answers.localUrl + ' should be good to start developing at. Have a good one, bye!');
	}
};

