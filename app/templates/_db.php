<?php
/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */
// !! tablePrefix can be no more than 5 characters !!
return array(
	'*' => array(
		'server'      => 'localhost',
		'user'        => '<%= dbUser %>',
		'password'    => '<%= dbPassword %>',
		'database'    => '<%= dbName %>',
		'tablePrefix' => '<%= dbPrefix %>'),
	'.no' => array(
		'server'			=> 'localhost')
	);
