var path = require('path');

var assets = './src'
var build = './app/public'
var buildAssets = build + '/assets'

module.exports = {
	paths: {
		SRC: '/assets',
		DIST: '/public/assets'
	},

	watch: {
		src: assets,
		scripts: 'js/**/*',
		styles: 'css/**/*.css'
	},

	serve: {
		development: {
			files: [
				buildAssets + '/js/main.js',
				buildAssets + '/css/styles.css'
			],
			proxy: '<%= localUrl %>',
			port: 9999,
			open: false,
			notify: {
				styles: ['display: hidden; padding: 12px; font-family: sans-serif; position: fixed; font-size: 14px; z-index: 10000; left: 0; bottom: 0; width: 200px; margin: 0; border-top-left-radius: 0; border-top-right-radius: 3px; color: #fff; text-align: center; background-color: rgba(0, 0, 0, 0.75);']
			}
		}
	},

	scripts: {
		src: assets + '/js/main.js',
		destination: buildAssets + '/js/',
		webpack:{
			entry: './src/js/main.js',
			output: {
				publicPath: '/assets/js/',
				filename: 'main.js',
			},
			resolve: {
				extensions: ['.js', '.jsx'],
				modules: [path.resolve(__dirname, '../src/js'), 'node_modules', 'web_modules']
			},
			module: {
				rules: [
					{
						test: /\.js?$/,
						exclude: /(node_modules|bower_components)/,
						use: [{
							loader: 'babel-loader',
							options: {
								presets: [['es2015', {modules: false}]],
								plugins: ['syntax-dynamic-import']
							},
						}],
					}

					// Loaders for other file types can go here
				],
			},
			plugins: []
		},
	},

	styles: {
		src: assets + '/css/styles.css',
		destination: buildAssets + '/css/',
		processors: {
			cssnext: {
				warnForDuplicates: false,
				browsers: [
					'last 2 versions'
				]
			}
		}
	},

	fonts: {
		src: assets + '/fonts/*.*',
		destination: buildAssets + '/fonts/'
	},

	clean: {
		src: buildAssets
	}

}
