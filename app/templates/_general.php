
<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

$protocol = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
$baseUrl    = $protocol . '://' . $_SERVER['SERVER_NAME'];
$basePath = dirname(dirname(__DIR__)) . '/app/public';

return array(
  '*' => array(
    'siteUrl'                   => '/',
    'cpTrigger'                 => '<%= adminPanel %>',
    'pageTrigger'               => 'side-',
    'devMode'                   => FALSE,
    'allowAutoUpdates' 			    => FALSE,
    'maxUploadFileSize'         => 16777216,
    'convertFilenamesToAscii' 	=> TRUE,
    'omitScriptNameInUrls'      => TRUE,
    'limitAutoSlugsToAscii'     => TRUE,
    'enableCsrfProtection'     	=> TRUE,
    'defaultWeekStartDay'       => 1,

    'serverName' => $_SERVER['HTTP_HOST'],

    'defaultSearchTermOptions'  => array(
      'subLeft'  => TRUE,
      'subRight' => TRUE
    ),

    'environmentVariables'      => array(
      'baseUrl'  => $baseUrl,
      'basePath' => $basePath
    ),
  ),

  '.dev' => array(
    'siteUrl'               => '/',
    'cpTrigger'             => 'admin',
    'devMode'               => TRUE,
    'allowAutoUpdates'      => TRUE,
    'cache'                 => FALSE,
    'cacheDuration'         => FALSE,
    'enableTemplateCaching' => FALSE,
    'userSessionDuration'	  => FALSE
  )

);
