// Define Dependencies
var config = require('../gulp.config').styles
var gulp = require('gulp')
var browserSync = require('browser-sync')
var sourcemaps = require('gulp-sourcemaps')
var plumber = require('gulp-plumber')
var rename = require('gulp-rename')

// Post CSS
var postcss = require('gulp-postcss')
var postcssImport = require('postcss-easy-import')
var url = require('postcss-url')
var cssNext = require('postcss-cssnext')
var reporter = require("postcss-reporter")
var cssnano = require('cssnano')
var rucksack = require('rucksack-css')

gulp.task('styles', function () {
	browserSync.notify('Rebuilding styles')
		var postCSS = [
			postcssImport(),
			url,
			rucksack(),
			cssNext(config.processors.cssnext),
			reporter({ clearMessages: true })
		]

		gulp.src(config.src)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(postcss(postCSS))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(config.destination))
		.pipe(browserSync.stream({match: '**/*.css'}))
})


gulp.task('styles:minify', function () {
	var postCSS = [
		postcssImport(),
		url,
		rucksack(),
		cssNext(config.processors.cssnext),
		cssnano(),
		reporter({ clearMessages: true })
	]

	return gulp.src(config.src)
		.pipe(plumber())
		.pipe(postcss(postCSS))
		.pipe(rename('styles.min.css'))
		.pipe(gulp.dest(config.destination))
})
