var config = require('../gulp.config').watch
var gulp = require('gulp')

gulp.task('watch:scripts', ['scripts', 'scripts:minify']) 
gulp.task('watch:styles', ['styles', 'styles:minify']) 

gulp.task('watch', ['serve'], function(){
    gulp.watch(config.scripts, {cwd: config.src}, ['watch:scripts'])
    gulp.watch(config.styles, {cwd: config.src}, ['watch:styles'])
})
