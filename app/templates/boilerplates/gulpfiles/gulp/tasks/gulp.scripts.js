var gulp = require('gulp')
var config = require('../gulp.config').scripts
var browserSync = require('browser-sync')

var bundleLogger = require('../utilities/gulp.logger')
var handleErrors = require('../utilities/gulp.errors')

var sourcemaps = require('gulp-sourcemaps')
var plumber = require('gulp-plumber')
var rename = require('gulp-rename')
var webpack = require('webpack-stream')

gulp.task('scripts', function() {
  return gulp.src(config.src)
		.pipe(plumber())
    .pipe(webpack( config.webpack ))
		.on('error', function(){
			this.emit('end');
		})
    .pipe(gulp.dest( config.destination ))
});


gulp.task('scripts:minify', function(){

	var prodConfig = Object.create(config.webpack)

	prodConfig.plugins = prodConfig.plugins.concat(
			new webpack.webpack.optimize.UglifyJsPlugin(),
			new webpack.webpack.optimize.AggressiveMergingPlugin()		
	)

  return gulp.src(config.src)
    .pipe(webpack( prodConfig ))
    .pipe(rename('main.min.js'))
		.on('error', function(){
			this.emit('end');
		})
    .pipe(gulp.dest( config.destination ))
})

