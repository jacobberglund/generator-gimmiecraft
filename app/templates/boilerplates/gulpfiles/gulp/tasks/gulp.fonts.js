// Define Dependencies
var config = require('../gulp.config').fonts
var gulp = require('gulp')

// Fonts (gulp fonts)
gulp.task('fonts', function () {
	return gulp.src(config.src)
		.pipe(gulp.dest(config.destination));
})
