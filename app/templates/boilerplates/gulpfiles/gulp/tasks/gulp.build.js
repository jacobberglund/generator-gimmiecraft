// Define Dependencies
var gulp = require('gulp')
var runSequence = require('run-sequence')

// Build (gulp build)
gulp.task('build', function (callback) {
  runSequence(
    'clean',
    [
      'scripts',
      'scripts:minify',
      'styles',
      'styles:minify',
			'fonts'
      // 'images',
      // 'icons',
      // 'favicons',
    ],
    callback
  )
})
