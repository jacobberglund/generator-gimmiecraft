<?php

// Path to your craft/ folder
define('CRAFT_LOCALE', 'no');

// Path to your craft/ folder
$craftPath = realpath(dirname(__FILE__) . '/../craft');


$serverName      = $_SERVER['SERVER_NAME'];
$serverNameParts = explode('.', $serverName);
$siteName        = $serverNameParts[0];
if ($siteName === 'www' && count($serverNameParts) > 1) {
	$siteName = $serverNameParts[1];
}

define('CRAFT_CONFIG_PATH', $craftPath . '/../config/');
define('CRAFT_TEMPLATES_PATH', $craftPath . '/../templates/');
define('CRAFT_PLUGINS_PATH', $craftPath . '/../plugins/');

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	if (function_exists('http_response_code'))
	{
		http_response_code(503);
	}

	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;
